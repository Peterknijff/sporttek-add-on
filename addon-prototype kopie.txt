#include <SPI.h>
#include <PN532_SPI.h>
#include "PN532.h"
#include <Ethernet.h>
#include <ArduinoJson.h>
#include <LiquidCrystal.h>

////////////////////////////////////////////////////////////////////////
// CONSTANTS
///////////////////////////////////////////////////////////////////////
#define DEBUGGING
#define PN532_CS 9
#define ETHNET_CS 10
byte mac[] = {  0x90, 0xA2, 0xDA, 0x00, 0x00, 0x00 };
byte client_server[] = { 192, 168, 192, 22 };   //Manual setup only
byte hostIP[] = { 192, 168, 192, 99 }; 
int client_port = 80;

String serial = "";
uint32_t xmitId=0;
uint32_t flowState;
String softwareVersion = "1.0";
String excerciseName = "Chest press";

////////////////////////////////////////////////////////////////////////
// PROGRAM STATES
////////////////////////////////////////////////////////////////////////
#define STATE_IDLE 0
#define STATE_INUSE 10
#define STATE_SYNC 20
#define STATE_RESPONSE 21 

////////////////////////////////////////////////////////////////////////
// VARIABLES
///////////////////////////////////////////////////////////////////////
String parameter = "?serial=";
String stateInitialized = "none";

unsigned long time;
unsigned long responseTime;

bool httpCall = false;
bool foundNFC = false;
bool reading = false;
bool proceed = true;
bool nameTrue = false;
bool valueTrue = false;
String jsonResponse = "";
int x = 0;

////////////////////////////////////////////////////////////////////////
// INIT
////////////////////////////////////////////////////////////////////////
// NFC SHIELD
PN532_SPI pn532spi(SPI, PN532_CS);
PN532 nfc(pn532spi);
// EHTERNET SHIELD
EthernetClient client;
// LCD DISPLAY
LiquidCrystal lcd(A13, A12, A11, A10, A9, A8); // 0, 1, 2, 3, 4, 5

void setup(void){  
  
  powerOn();
  
  flowState = STATE_IDLE;
}

//RUNTIME functions
void powerOn(){

#ifdef DEBUGGING
  Serial.begin(19200);
  Serial.println("==========================================");
  Serial.println("=========[Sport-tek Add-on "+softwareVersion+"]===========");
  Serial.println("==========================================");
#endif

  // Pinmodes
  pinMode (ETHNET_CS, OUTPUT); 
  pinMode (PN532_CS, OUTPUT); 

  lcdBootup();  
  initConnections();

}
void initConnections(){
  // Disable NFC SPI, activate ETHERNET SPI
  spiSelect(ETHNET_CS);
  
  // INITIALIZE ETHERNET
  #ifdef DEBUGGING
    Serial.println("Sending DHCP request");
  #endif
  if (Ethernet.begin(mac) == 0){
    Ethernet.begin(mac, hostIP);    
    #ifdef DEBUGGING
      Serial.println("Sending DHCP request");
      Serial.println("Failed to configure Ethernet using DHCP");
      Serial.println("Setting static IP address instead");
      Serial.print("Arduino IP is ");
      Serial.println(Ethernet.localIP());
    #endif
  }else{
    #ifdef DEBUGGING
      Serial.println("Obtained DHCP lease"); 
      Serial.print("Arduino IP is ");
      Serial.println(Ethernet.localIP());     
    #endif
  }

  // Disable ETHERNET SPI, activate NFC SPI
  spiSelect(PN532_CS);

  // INITIALIZE NFC
  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata){
    #ifdef DEBUGGING
      Serial.println("NFC INIT FAILED");
    #endif
    while (1); // halt
  }else{
    #ifdef DEBUGGING
      Serial.println("NFC INIT SUCCESS");
    #endif
    nfc.SAMConfig();
  }
  #ifdef DEBUGGING
    Serial.println("NFC and Ethernet initialised OK");
  #endif
}
void spiSelect(int CS){
  // disable all SPI
  digitalWrite(PN532_CS,HIGH);
  digitalWrite(ETHNET_CS,HIGH);
  // enable the chip we want
  digitalWrite(CS,LOW);  
  if(CS == ETHNET_CS){
    // ETHERNET uses MSB first
    SPI.setBitOrder(MSBFIRST);
    #ifdef DEBUGGING
      Serial.println("Select Ethernet SPI");
    #endif
  }else{
    // NFC uses LSB first
    SPI.setBitOrder(LSBFIRST);
    #ifdef DEBUGGING
      Serial.println("Select NFC SPI");
    #endif
  }
}
void readNFC(){
  Serial.println(digitalRead(ETHNET_CS) == LOW);
  if (digitalRead(ETHNET_CS) == HIGH){
      spiSelect(PN532_CS);
  } 
  
  uint8_t success = false;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  
  /* Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  'uid' will be populated with the UID, and uidLength will indicate
  if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight) */
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, 2000);
        
  #ifdef DEBUGGING
    Serial.println("READNFC FUNCTION VOORAF");
//    Serial.println(foundNFC);
//    Serial.println(success);
//    Serial.println(uidLength);
  #endif
  
  if(success){  
    #ifdef DEBUGGING
    Serial.println("Found tag");    
    nfc.PrintHex(uid, uidLength);    
    #endif
    
    // We only accept Mifare Ultralight tags (7 byte UID)
    if (uidLength == 7){  
      String totalString;
      // Convert bytes to string        
      for (uint8_t i = 0; i < uidLength; i++){
        if(!uid[i] < 0x10 && i != 0){
          totalString.concat(":");
        }else{
          totalString.concat("0");
        }
        totalString.concat(String(uid[i], HEX));
      }
      serial = totalString;
      #ifdef DEBUGGING
        Serial.print("Serial: ");
        Serial.print(serial);
      #endif
      foundNFC = true;
    }else{
      #ifdef DEBUGGING
        Serial.println("niks");
      #endif
      foundNFC = false;
    }
  }else{
    foundNFC = false;
  }
}
void resetSerial(){
  serial = "";
}
//LCD functions
void lcdBootup(){
  lcd.clear();
  lcd.begin(20, 4);
  lcd.setCursor(0,0);
  lcd.print("Sport-tek add-on");
  delay(1200);
  lcd.setCursor(0,1);
  lcd.print("Version "+softwareVersion);
  lcd.setCursor(0,2);
  lcd.print("by: Peter Knijff");
  delay(1200);
  lcd.setCursor(0,3);
  lcd.print("Loading");
  for (int i=0; i <= 12; i++){
    delay(i*25);
    lcd.print(".");
  } 
  delay(1000);
}
void lcdError(String msg1="", String msg2="", String msg3=""){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Error");

 if(msg1 != ""){
    lcd.setCursor(0,1);
    lcd.print(msg1);
  }
  if(msg2 != ""){
    lcd.setCursor(0,2);
    lcd.print(msg2);
  }
  if(msg3 != ""){
    lcd.setCursor(0,3);
    lcd.print(msg3);
  }
}
void lcdMsg(String msg1="", String msg2="", String msg3="", String msg4=""){
  lcd.clear();
  if(msg1 != ""){
    lcd.setCursor(0,0);
    lcd.print(msg1);
  }
  if(msg2 != ""){
    lcd.setCursor(0,1);
    lcd.print(msg2);
  }
  if(msg3 != ""){
    lcd.setCursor(0,2);
    lcd.print(msg3);
  }
  if(msg4 != ""){
    lcd.setCursor(0,3);
    lcd.print(msg4);
  }
}
//ETHERNET functions
JsonObject& request(String type, String param = ""){

  httpCall = true;

  if (digitalRead(PN532_CS) == LOW){
      spiSelect(ETHNET_CS);
  } 
  
  // Make HTTP GET API call
  if (client.connect(client_server, client_port)){
      #ifdef DEBUGGING     
        Serial.println(F("Making HTTP Call"));
      #endif
      if(type == "username"){
        #ifdef DEBUGGING
          Serial.println("Username met: "+serial);
        #endif
        client.println("GET /api/v1/username.json?serial="+serial+" HTTP/1.1");
      }else if(type == "sync"){
        #ifdef DEBUGGING
          Serial.println("Sync met: "+param);
        #endif
      //      client.println("POST /api/v1/sync.json?serial="+serial+"&workout="+workout+"&data="+data+" HTTP/1.1");
      }
      client.println(F("Host: 192.168.192.22"));
      client.println("User-Agent: Arduino/1.0");
      client.println("Connection: close");
      client.println("Content-Type: application/x-www-form-urlencoded;");
      client.print("Content-Length: ");
      String totalParam = parameter+serial;
      client.println(totalParam.length());
      client.println();
      client.println(totalParam);    
      
      responseTime = millis();
      
      while(client.connected()){
        if ((millis() - responseTime)>1000) {   
          return handleRequest();
        }
      }

   } else {
      #ifdef DEBUGGING
        Serial.println("Connection to server failed");
      #endif
  }
}
JsonObject& handleRequest(){
  if ((millis() - responseTime)>1000) {   
    if (client.available()) {
      while(client.available()){
        char c = client.read();
         if(c == '{'){
            reading = true;
          }
          
          if(reading && proceed){
            jsonResponse += c;
          }
          
          if(c == '}'){
            proceed = false;            
          }
        }
  
        StaticJsonBuffer<100> jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(jsonResponse);
        
        client.stop();
        httpCall = false;
        return root;
    }else{
      #ifdef DEBUGGING
        Serial.println("Client is nooot available");
      #endif
    }
  }
}

//ACTION functions
bool handleCheckIn(){
  // Request user from API with UID
  JsonObject& response = request("username");
  
  int responseCode = response["code"];
  const char* firstname = response["firstname"];

  if(responseCode == 200){
    //Start session
    startSession(firstname);
    return true;
  }else if (responseCode == 404){
    lcdError("Sorry, no user was","found for this", "bracelet.");
    return false;
  }else{
    lcdError("Sorry,","something went wrong.");
    return false;
  }
}
void startSession(String username){
  String workout = "3x 8 reptitions";
  lcdMsg("Welkom "+username, excerciseName, workout, "You can now start...");
}
void getState(){
  
}

void loop(void){
  // STATE IDLE, action: CHECK FOR NFC TAG
  if(flowState == STATE_IDLE){    
    // Initialize this state
    if(stateInitialized != "idle"){
      stateInitialized = "idle";
      resetSerial();
      // Select NFC pin as active
      delay(1000);
      lcdMsg("Ready", "Scan bracelet", "to check in");
      #ifdef DEBUGGING
        Serial.println("initIdle");
      #endif
    }

    #ifdef DEBUGGING
      Serial.println("IDLE");
    #endif

    if(!httpCall){
      readNFC();
      if(foundNFC){
        #ifdef DEBUGGING
          Serial.println("ReadingNFCSuccess");
        #endif
        lcdMsg("Loading...");
        if(serial != ""){
          if(handleCheckIn()){
            flowState = STATE_INUSE;  
          }else{
            flowState = STATE_IDLE;
            stateInitialized = "";
          }
        }else{
          lcdMsg("This tagtype is", "nog allowed.");
        }
      }else{
        #ifdef DEBUGGING
          Serial.println("NO NFC TAG PRESENT");
        #endif
      }
    }
  }
  
  // STATE IN-USE, action: RECORD WORKOUT
  if(flowState == STATE_INUSE){
     // Initialize this state
    if(stateInitialized != "inuse"){
      stateInitialized = "inuse";    

      #ifdef DEBUGGING
        Serial.println("init in use");
      #endif
    }
    
    // Record movement (if movement is far away).
    if(!httpCall){
      readNFC();
    }
  }
  // STATE SYNC, action: SEND WORKOUT DATA TO SERVER
  if(flowState == STATE_SYNC){

  }
}
