#include <SPI.h>
#include <PN532_SPI.h>
#include "PN532.h"
#include <Ethernet.h>
#include <ArduinoJson.h>
#include <LiquidCrystal.h>
#include <LcdProgressBarDouble.h>

////////////////////////////////////////////////////////////////////////
// CONSTANTS
///////////////////////////////////////////////////////////////////////
#define DEBUGGING // General debugging
#define NFCDEBUG // Init + action with NFC 
#define ETHDEBUG // Init + action with Ethernet 
#define PROGRESSDEBUG // LCD visualisation of meters & measurements
#define HTTPDEBUG // Requests to the API over HTTP protocol
#define WORKOUTDEBUG // Workout

byte mac[] = {  0x90, 0xA2, 0xDA, 0x00, 0x00, 0x00 };
const char* server = "192.168.192.22";  // server's address
String userResource = "GET /api/v1/username.json?serial=";
String syncResource = "POST /app_dev.php/api/v1/syncs.json HTTP/1.1";
byte hostIP[] = { 192, 168, 192, 99 };
int client_port = 80;

String serialString = "";
char firstnameString[32];

//Weight steps in KG
int weightStep = 5;
// Weight thickness in mm
int weightThickness = 25;
// Amount of weights in pcs
int totalWeights = 6;
// Margin calculated from floor to bottom of first weight in mm
int weightsMargin = 30;
// movingspace is the space where the stack can move in, top stack to bottom of the top part in mm 
int movingSpace = 150;
// measurementMargin in mm, this is because of the inaccuracy in mm's of the measurement
int measurementMargin = 6;

uint32_t flowState;
String softwareVersion = "v1.3";
String exerciseName = "Chest press";
String exerciseData = "";

// INPUTS
const int echoPin = A14; // Ultrasonic input
static const uint8_t input_pins[4] = {echoPin};
// OUTPUTS
const int PN532_CS = 9; // NFC output
const int ETHNET_CS = 10; // Ethernet output
const int trigPin = A15;  // Ultrasonic output
static const uint8_t output_pins[4] = {PN532_CS, ETHNET_CS, trigPin};

////////////////////////////////////////////////////////////////////////
// PROGRAM STATES
////////////////////////////////////////////////////////////////////////
#define STATE_IDLE 0
#define STATE_INUSE 10
#define STATE_SYNC 20
#define STATE_RESPONSE 21

////////////////////////////////////////////////////////////////////////
// VARIABLES
///////////////////////////////////////////////////////////////////////
String parameter = "?serial=";
String stateInitialized = "none";

unsigned long restTimer;
unsigned long keepPinTimer;
unsigned long workoutTimer;
unsigned long workoutProgressTimer;
unsigned long startedMillis;
unsigned long reversedMillis;
unsigned long drawMillis = 0;
unsigned long prevMillis = 0;
bool timerBackForth = false;

const unsigned long HTTP_TIMEOUT = 600;  // max respone time from server
const size_t MAX_CONTENT_SIZE = 512;       // max size of the HTTP response

bool timer = false;
bool httpCall = false;
bool foundNFC = false;
bool reading = false;
bool proceed = true;
bool nameTrue = false;
bool valueTrue = false;
bool workoutActive = false;
bool initNFC = false;
bool initETH = false;
byte lcdNumCols = 20; // -- number of columns in the LCD

int pinCountDown = 0;
bool switchDistanceInfo = false;
char weight = 0;
char reps = 0;
char sets = 0;
bool up = false;
bool skipCount = true;
bool isRest = false;

long previousDistance = 0;
long repHighest = 0;
long repLowest = 0;
long restDuration = 30;
long exerciseDuration = 2000;
int repCount = 0;
int setCount = 0;
////////////////////////////////////////////////////////////////////////
// INIT
////////////////////////////////////////////////////////////////////////
// NFC SHIELD
PN532_SPI pn532spi(SPI, PN532_CS);
PN532 nfc(pn532spi);
// EHTERNET SHIELD
EthernetClient client;
// LCD DISPLAY
LiquidCrystal lcd(A13, A12, A11, A10, A9, A8);  // 0, 1, 2, 3, 4, 5
LcdProgressBarDouble lpg(&lcd, 3, lcdNumCols);

// The type of data that we want to extract from the page
struct UserData {
  char code[32];
  char weight[32];
  char sets[32];
  char reps[32];
  char firstname[32];
};
long getDistance() {
  long duration, distance;
  digitalWrite(trigPin, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(5); // Added this line
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  return microsecondsToCentimeters(duration)-weightsMargin;
}
long microsecondsToCentimeters(long microseconds) {
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 2.9 / 2;
}
void lcdClear() {
  lcd.clear();
}
void lcdBootup() {
  lcdClear();
  delay(100);
  lcd.begin(lcdNumCols, 4);
  lcd.setCursor(0, 0);
  lcd.print("Sport-tek add-on");
  delay(1200);
  lcd.setCursor(0, 1);
  lcd.print("Release " + softwareVersion);
  lcd.setCursor(0, 2);
  lcd.print("by: Peter Knijff");
  delay(1200);
  lcd.setCursor(0, 3);
  lcd.print("Loading");
  for (int i = 0; i <= 12; i++) {
    delay(i * 25);
    lcd.print(".");
  }
  delay(1000);
}
void lcdError(String msg1 = "", String msg2 = "", String msg3 = "") {
  lcdClear();
  delay(100);
  lcd.setCursor(0, 0);
  lcd.print("Error!");

  if (msg1 != "") {
    lcd.setCursor(0, 1);
    lcd.print(msg1);
  }
  if (msg2 != "") {
    lcd.setCursor(0, 2);
    lcd.print(msg2);
  }
  if (msg3 != "") {
    lcd.setCursor(0, 3);
    lcd.print(msg3);
  }
}
void lcdMsg(String msg1 = "", String msg2 = "", String msg3 = "", String msg4 = "") {
  lcdClear();
  delay(100);
  if (msg1 != "") {
    lcd.setCursor(0, 0);
    lcd.print(msg1);
  }
  if (msg2 != "") {
    lcd.setCursor(0, 1);
    lcd.print(msg2);
  }
  if (msg3 != "") {
    lcd.setCursor(0, 2);
    lcd.print(msg3);
  }
  if (msg4 != "") {
    lcd.setCursor(0, 3);
    lcd.print(msg4);
  }
}
void lcdMsgLine(int line, int point = 0, String msg = "") {
  if (point > 0) {
    clearPoint(line, point);
    lcd.setCursor (point, line);
    lcd.print(msg);
  } else {
    clearLine(line);
    lcd.setCursor (0, line);
    lcd.print(msg);
  }
}
void clearLine(int line) {
  if (line > 0 && line <= 4) {
    lcd.setCursor(0, line);
    lcd.print("                    ");
    lcd.setCursor(0, line);
  }
}
void clearPoint(int line, int point) {
  lcd.setCursor(point, line);
  lcd.print(" ");
  lcd.setCursor(point, line);
}
void setup(void) {
  powerOn();
  flowState = STATE_IDLE;
}
//-- initializing the progress bars
//RUNTIME functions
void powerOn() {
  Serial.begin(19200);
  Serial.println("==========================================");
  Serial.println("=========[Sport-tek Add-on " + softwareVersion + "]==========");
  Serial.println("==========================================");

  // Pinmodes
  // INPUT PINS
  for (int i = 0; i < sizeof(input_pins); i++) {
    pinMode(input_pins[i], INPUT);
  }
  // OUTPUT PINS
  for (int i = 0; i < sizeof(output_pins); i++) {
    pinMode(output_pins[i], OUTPUT);
  }

  digitalWrite(PN532_CS, LOW);
  digitalWrite(ETHNET_CS, HIGH);

  lcdBootup();
  initPN532();
  initETHNET();
  
  if (initNFC && initETH) {
    lcdMsg("Success!", "NFC initialized", "Conn. with network", "IP: " + DisplayAddress(Ethernet.localIP()));
  } else {
    lcdError("", "Something went wrong", "while initializing.");
    while (1); // halt
  }
  delay(3000);
}
String DisplayAddress(IPAddress address) {
  return String(address[0]) + "." +
         String(address[1]) + "." +
         String(address[2]) + "." +
         String(address[3]);
}
void initETHNET() {
  //Initialise Ethernet connection
  Serial.println("Start Ethernet");
  enableETH();

   // INITIALIZE ETHERNET
  #ifdef ETHDEBUG
    Serial.println("Sending DHCP request");
  #endif
  if (Ethernet.begin(mac, 5000) == 0) {
    initETH = false;
  #ifdef ETHDEBUG
    Serial.println("Failed to configure Ethernet using DHCP");
    Serial.println(Ethernet.localIP());
  #endif
  } else {
    initETH = true;
  #ifdef ETHDEBUG
    Serial.println("Obtained DHCP lease");
  #endif
  }
  
  #ifdef DEBUGGING
  Serial.println("NFC and Ethernet initialised OK");
  #endif
  
}

void initPN532() {
  nfc.begin();
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    initNFC = false;
  }else{
    initNFC = true;
  }
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); 
  Serial.println((versiondata>>24) & 0xFF, HEX); 
  Serial.print("Firmware ver. "); 
  Serial.print((versiondata>>16) & 0xFF, DEC); 
  Serial.print('.'); 
  Serial.println((versiondata>>8) & 0xFF, DEC);
  
  // configure board to read RFID tags
  nfc.SAMConfig();
  Serial.println("Waiting for an ISO14443A Card ...");
}
void enablePN() {  
  digitalWrite(ETHNET_CS, HIGH);
  digitalWrite(PN532_CS, LOW);
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(LSBFIRST);
  SPI.setClockDivider(SPCR & SPI_CLOCK_MASK);
  delay(10);
}
void enableETH() {
  digitalWrite(ETHNET_CS, LOW);
  digitalWrite(PN532_CS, HIGH);
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV4); 
  SPI.setDataMode(SPCR & SPI_MODE_MASK);
  SPCR &= ~(_BV(DORD));
  SPI.setClockDivider( SPCR & SPI_CLOCK_MASK);
  delay(10);
}
bool readNFC(String activeSerial = "") {
 uint8_t success;
  uint8_t UID[] = { 0, 0, 0, 0, 0, 0, 0 };
  uint8_t uidLength;
    
  Serial.println("NFC Read");
  enablePN();
  nfc.begin();
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, UID, &uidLength);
  
  if (success) {
    #ifdef NFCDEBUG
    Serial.println("Found an ISO14443A card");
    Serial.print("  UID Length: ");
    Serial.print(uidLength, DEC);
    Serial.println(" bytes");
    Serial.print("  UID Value: ");
    nfc.PrintHex(UID, uidLength);
    Serial.println("");
    #endif
    
    if (uidLength == 7 || uidLength == 4) {
      // Convert bytes to string        
      String totalString;
      for (uint8_t i = 0; i < uidLength; i++){
        if(!UID[i] < 0x10 && i != 0){
          totalString.concat(":");
        }else{
          totalString.concat("0");
        }
        totalString.concat(String(UID[i], HEX));
      }
      #ifdef NFCDEBUG
      Serial.print("UIDLENGTH is 4 of 7, totalString is:");
      Serial.println(totalString);
      #endif
      if(uidLength == 4){
        totalString = "08:4f:1c:6b";
      }
      if (activeSerial == "") {
        serialString = totalString;
        return true;
      } else {
        if (totalString == activeSerial) {
          return true;
        } else {
          return false;
        }
      }
    } else {
      return false;
    }
  }else{
    return false;
  }
}
void resetSession() {
  exerciseData = "";
  isRest = false;
  skipCount = true;
  up = false;
  setCount = 0;
  repCount = 0;
  repHighest = 0;
  repLowest = 0;
  previousDistance = 0;
  workoutActive = false;
  pinCountDown = 0;
  weight = 0;
  sets = 0;
  reps = 0;
  serialString = "";
  char firstnameString[32];
}
// LCD functions
bool parseUserData(char* content, struct UserData* userData) {

  // Allocate a temporary memory pool on the stack
  StaticJsonBuffer<100> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(content);

  if (!root.success() || int(root["code"]) != 200) {
    #ifdef HTTPDEBUG
    Serial.println("JSON parsing failed!");
    #endif
    return false;
  }

  restDuration = root["rest"];

  // Here were copy the strings we're interested in
  strcpy(userData->code, root["code"]);
  strcpy(userData->weight, root["weight"]);
  strcpy(userData->sets, root["sets"]);
  strcpy(userData->reps, root["reps"]);
  strcpy(userData->firstname, root["firstname"]);
  strcpy(firstnameString, root["firstname"]);
  // It's not mandatory to make a copy, you could just use the pointers
  // Since, they are pointing inside the "content" buffer, so you need to make
  // sure it's still in memory when you read the string

  return true;
}
//HTTP functions
bool handleCheckIn() {
  digitalWrite(10, LOW); //SPI select Ethernet
  digitalWrite(9, HIGH); //SPI deselect NFC
   
  if (connect(server)) {
    if (sendRequest(server, userResource + serialString) && skipResponseHeaders()) {
      char response[MAX_CONTENT_SIZE];
      readReponseContent(response, sizeof(response));
      disconnect();
      UserData userData;
      if (parseUserData(response, &userData)) {
        if(startSession(&userData)){
          return true;
        }else{
          return false;
        }
      } else {
        return false;
      }
    } else {
      disconnect();
      return false;
    }
  } else {
    disconnect();
    return false;
  }
}
bool connect(const char* hostName) {
  #ifdef HTTPDEBUG
    Serial.print("Connect to ");
    Serial.println(hostName);
  #endif
  client.stop();
  bool ok = client.connect(hostName, 80);

  #ifdef HTTPDEBUG
  Serial.println(ok ? "Connected" : "Connection Failed!");
  #endif
  return ok;
}
void disconnect() {
  #ifdef HTTPDEBUG
  Serial.println("Disconnect");
  #endif
  client.stop();
}
bool sendRequest(const char* host, String resource) {
  #ifdef HTTPDEBUG
  Serial.println(resource);
  #endif
  client.print(resource);
  client.println(" HTTP/1.1");
  client.print("Host: ");
  client.println(server);
  client.println("Connection: close");
  client.println();
  return true;
}
bool sendRequest(const char* host, String resource, JsonObject& json) {
  client.println(resource);
  client.println("Host: " +String(server));
  client.println("User-Agent: Arduino/1.0");
  client.println("Content-Type: application/x-www-urlencoded");
  client.print("Content-Length: ");
  client.println(json.measureLength());
  client.println("Connection: close");
  client.println();
  json.printTo(client);
  return true;
}
bool skipResponseHeaders() {
  // HTTP headers end with an empty line
  char endOfHeaders[] = "\r\n\r\n";

  client.setTimeout(HTTP_TIMEOUT);
  bool ok = client.find(endOfHeaders);

  if (!ok) {
    #ifdef HTTPDEBUG
    Serial.println("No response or invalid response!");
    #endif
  }
  return ok;
}
void readReponseContent(char* content, size_t maxSize) {
  size_t length = client.readBytes(content, maxSize);
  content[length] = 0;
  #ifdef HTTPDEBUG
  Serial.println(content);
  #endif
}
void handleCheckOut() {
  lcdMsg("Checking out.", "", "Please one moment...");
  delay(1200);
  flowState = STATE_IDLE;
  resetSession();
}
bool startSession(struct UserData* userData) {
  flowState = STATE_INUSE;

  weight =  atoi(userData->weight);
  sets =    atoi(userData->sets);
  reps =    atoi(userData->reps);

  String endOfWeight = String(userData->weight).substring(String(userData->weight).length()-1);
  
  if(endOfWeight == "0" || endOfWeight == "5"){
    String workout = String(userData->sets) + " set(s) of " + String(userData->reps) + " reps";
    lcdMsg("Welcome " + String(userData->firstname) + "!", exerciseName, workout, "Put pin into " + String(userData->weight) + "kg");
    delay(5000);
    return true;
  }else{
    lcdError("Invalid weight for", "this exercise.", "Contact coach.");
    delay(2000);
    return false;
  }
}
int freeRam () {
  extern int __heap_start, *__brkval;
  int v;
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}
bool syncWorkout() {
  // Inside the brackets, 500 is the size of the pool in bytes.
  // If the JSON object is more complex, you need to increase that value.
  //  StaticJsonBuffer<500> jsonBuffer;
  #ifdef DEBUGGING
  Serial.print("FREERAM");
  Serial.println(freeRam());
  #endif
  
  DynamicJsonBuffer jsonBuffer;
  JsonObject& responseJson = jsonBuffer.parseObject(exerciseData);
  responseJson["serial"] = serialString;
  responseJson["exercise"] = exerciseName;

  #ifdef SYNCDEBUG
  Serial.print("Size of JSON: ");
  Serial.println(responseJson.measureLength());
  Serial.print("JSON success: ");
  Serial.println(responseJson.success());
  #endif
  
  if (responseJson.success()) {
    // Send data to server
    if (connect(server)) {
      String syncing = "Syncing";
      lcdMsg("Well done " + String(firstnameString) +"!", "Please one moment", "saving your workout.");
        for (int i = 0; i <= 19; i++) {
          String value = "";
          if(i < syncing.length()){
            value = syncing[i];
          }else{
            value = ".";
          }
          delay(i * 18 - i);
          lcdMsgLine(3,i, value);
        }
      if (sendRequest(server, syncResource, responseJson) && skipResponseHeaders()) {
        char response[MAX_CONTENT_SIZE];
        readReponseContent(response, sizeof(response));
        #ifdef SYNCDEBUG
        Serial.print("Response: ");
        Serial.println(response);
        #endif
        disconnect();
        return true;
      } else {
        disconnect();
        return false;
      }
    } else {
      disconnect();
      return false;
    }
  } else {
    return false;
  }
}
int getTotalHeight(){
    return totalWeights * weightThickness;
}
int getPinHeight(){
    return getTotalHeight() - ((int(weight) / weightStep * weightThickness)-weightThickness/2);
}
bool around(long distance) {
  int pinHeight = getPinHeight();
  unsigned long currentMillis = millis();
  
  if (abs(distance - pinHeight) <= int(5)) {
    if (currentMillis - keepPinTimer >= 1000) {
      pinCountDown++;
      keepPinTimer = currentMillis;
      if (pinCountDown == 6) {
        return true;
      } else {
        lcdClear();
        lcdMsgLine(0, 0, "Please keep the pin,");
        lcdMsgLine(1, 0, "in this weight for:");
        lcdMsgLine(2, 0, String(6 - pinCountDown) + "sec");
        return false;
      }
    }
  } else { 
    if (pinCountDown > 0) {
      lcdMsg("Please put the pin", "in weight: " + String(int(weight)) + "kg", "@ height: "+String(float(pinHeight)*1.0/10)+"cm");
      // Reset countdown value
      pinCountDown = 0;
    }
    
    if (currentMillis - keepPinTimer >= 2000 && !switchDistanceInfo) {
      lcdMsgLine(3,0, "Pin: "+String(float(distance)*1.0/10)+"cm");
      keepPinTimer = currentMillis;
      switchDistanceInfo = true;
    }else if(currentMillis - keepPinTimer >= 2000 && switchDistanceInfo){
      lcdMsgLine(3,0, "Weight: "+String(float(pinHeight)*1.0/10)+"cm");
      keepPinTimer = currentMillis;
      switchDistanceInfo = false;
    }
      return false;
  }
}
bool initLpg(int indicators, int pinHeight){
  
  #ifdef PROGRESSDEBUG
  Serial.print("INIT LPG....");
  Serial.print("pinHeight: ");
  Serial.println(pinHeight);
  Serial.print("Exercise duration: ");
  Serial.println(exerciseDuration);
  #endif

  if(indicators == 1){
    //-- Set min and max values of only one bar
    lpg.setRangeValue1(0, exerciseDuration);
  }else if (indicators == 2){
    lpg.setRangeValue2(pinHeight, pinHeight+movingSpace);
  }
  return true;
}
void showProgress(int indicators, long distance, unsigned long currentMillis){  
  #ifdef PROGRESSDEBUG
  Serial.println("SHOWPOGRESSFUNCTION");
  #endif
  int totalHeight = getTotalHeight();
  int pinHeight = getPinHeight();
  // calc where current distsance is between totalspace
  
   if (currentMillis - workoutProgressTimer >= exerciseDuration && !up) {
    workoutProgressTimer = currentMillis;
    up = true;
    #ifdef PROGRESSDEBUG
       Serial.println("UP");
    #endif
  }else if(currentMillis - workoutProgressTimer >= exerciseDuration+100 && up){
    workoutProgressTimer = currentMillis;
    up = false;
   #ifdef PROGRESSDEBUG
       Serial.println("DOWN");
    #endif
  }

  if(up){
    drawMillis = currentMillis-workoutProgressTimer;
  }else{
    drawMillis = exerciseDuration+((currentMillis-workoutProgressTimer)*(-1));
  }

   #ifdef PROGRESSDEBUG
   Serial.print("------TIMER: ");
   Serial.println(drawMillis);
   Serial.print("---DISTANCE: ");
   Serial.println(distance);
    #endif

  if(indicators == 1){
    lpg.draw(drawMillis);
  }else if (indicators ==2){
    lpg.draw(drawMillis, distance);
  }
   
}
void addRepLcd(int value) {
  // Point on LCD screen
  int point = 6;
  if (reps > 9 && repCount < 10) {
    point++;
  }
  lcdMsgLine(2, point, String(value));
}
void startMeasurements(long distance) {
  #ifdef DEBUGGING
  Serial.print("______FREEE RAM BEFORE MEASUREMENT LOOP");
  Serial.println(freeRam());
  #endif
  
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(exerciseData);
  JsonObject& jsonSets = json["sets"].as<JsonObject>();
  
  #ifdef WORKOUTDEBUG
  Serial.print("SETCOUNTTT");
  Serial.println(setCount);
  #endif
  /* Checks if setCount during measuring is smaller
    or equal to the amount of sets from exercise */
  if (setCount <= sets - 1) {
    // Get root of current set in order to add repetition data
    String setje = json["sets"]["set" + String(setCount + 1)];
    if (setje.length() == 0) {
      JsonObject& jsonSet = jsonSets.createNestedObject("set" + String(setCount + 1));
    }
    JsonObject& jsonSet = json["sets"]["set" + String(setCount + 1)].as<JsonObject>();

    // If rest is active, exercise is on hold
    if (!isRest) {
      /* Checks if repCount during measuring is smaller
        or equal to the amount of sets from exercise */
      if (repCount <= reps - 1) {
        JsonObject& jsonRep = jsonSet.createNestedObject("rep" + String(repCount + 1));
        // Check if the movement is increasing, for better measurements a margin is deducted
        if (distance - previousDistance > measurementMargin) {
          // Set repetition timer, in order to measure repetition time
          if (!up) {
            if (!skipCount) {
              // Count reps in between reps larger and smaller than 0/sets
              if (repCount != reps - 1) {
                repCount++;
                jsonRep["highest"] = repHighest;
                jsonRep["lowest"] = repLowest;
                jsonRep["time"] = millis() - workoutTimer;
              } else {
                skipCount = true;
              }
            }
            up = true;
            workoutTimer = millis();
          }
          #ifdef WORKOUTDEBUG
          Serial.println("----------------UP");
          #endif
          // Set previous distance
          previousDistance = repHighest = distance;
          // Check if the movement is decreasing
        } else if (distance - previousDistance < (measurementMargin * (-1))) {
          #ifdef WORKOUTDEBUG
          Serial.println("distance");
          Serial.println(distance);
          Serial.println("previousDistance");
          Serial.println(previousDistance);
          #endif
          if (up) {
            if (skipCount) {
              // Count reps in for first and last
              if (repCount == 0 || repCount == reps - 1) {
                repCount++;
                addRepLcd(repCount);
                // Add reptition with the correct count set object
                jsonRep["highest"] = repHighest;
                jsonRep["lowest"] = repLowest;
                jsonRep["time"] = millis() - workoutTimer;
                skipCount = false;
              }
            }
            up = false;
          }
          if (distance < repLowest) {
            // Set previous distance
            addRepLcd(repCount);
          }
          previousDistance = repLowest = distance;


        }
        exerciseData = "";
        json.printTo(exerciseData);
      } else {
        repCount = 0;
        skipCount = false;
        setCount++;

        lcdMsgLine(1, 6, String(setCount));
        if (reps > 9) {
          lcdMsgLine(2, 6, "00");
        }
        lcdMsgLine(2, 6, "0");
        isRest = true;
        pinCountDown = 0;
      }
    } else if (pinCountDown < restDuration) {
      lcdMsgLine(3, 0, "Take rest for: " + String(restDuration - pinCountDown++) + "sec");
      delay(1000);
      if (pinCountDown == restDuration) {
        lcdMsgLine(3, 0, "");
        isRest = false;
        pinCountDown = 0;
      }
    }
  } else {
    if (syncWorkout()) {
      handleCheckOut();
    } else {
      lcdError("Something went wrong", "while syncing.", "Trying again");
      delay(1000);
    }
  }
  #ifdef DEBUGGING
  Serial.print("______FREEE RAM AFTER MEASUREMENT LOOP");
  Serial.println(freeRam());
  #endif
}
void recordWorkout() {
  long distance = getDistance();  
  if (workoutActive) {
    if (!timer) {
//        initLpg(2, int(getPinHeight()));
        workoutTimer = millis();
        timer = true;
    }else{
      startMeasurements(distance);
//      unsigned long currentMillis = millis();
//      if(currentMillis - prevMillis){
//        prevMillis = currentMillis;
//        showProgress(2, distance, currentMillis);
//      }
    }
  } else {
    if (around(distance) && !workoutActive) {
      workoutActive = true;
      String digits = "";
      if (int(reps) > 9) {
        digits = "00";
      } else {
        digits = "0";
      }
      StaticJsonBuffer<100> jsonBuffer;
      JsonObject& root = jsonBuffer.createObject();
      JsonObject& jsonSets = root.createNestedObject("sets");
      // Print the JSON to the exerciseData String
      root.printTo(exerciseData);
      lcdMsg("Workout in progress", "Sets: 0|" + String(int(sets)), "Reps: " + digits + "|" + String(int(reps)));
      // Set repLowest and Highest to current distance
      repLowest = repHighest = previousDistance = distance;
    }
  }
}
void loop(void) {
  // STATE IDLE, action: CHECK FOR NFC TAG
  if (flowState == STATE_IDLE) {
    // Initialize this state
    if (stateInitialized != "idle") {
      stateInitialized = "idle";
      // Select NFC pin as active
      delay(1000);
      lcdMsg("Ready!", "", "Scan bracelet in", "order to check in.");
      #ifdef DEBUGGING
      Serial.println("initIdle");
      #endif
    }

    #ifdef DEBUGGING
    Serial.println("IDLE");
    #endif

    if (!httpCall) {
      if (readNFC()) {

        #ifdef NFCDEBUG        
        Serial.print("NFC success: serialString is: ");
        Serial.println(serialString);
        #endif
        lcdMsg("Loading...");
        if (serialString != "") {
          if (!handleCheckIn()) {
            lcdError("Something went wrong", "while checking in,", "please try again.");
            delay(3500);
            flowState = STATE_IDLE;
            stateInitialized = "";
          }
        } else {
          lcdMsg("This tagtype is", "nog allowed.");
        }
      }else{
        #ifdef NFCDEBUG
        Serial.print("readNFC returned false");
        #endif
      }
    }
  }
  // STATE IN-USE, action: RECORD WORKOUT
  if (flowState == STATE_INUSE) {

    // Initialize this state
    if (stateInitialized != "inuse") {
      stateInitialized = "inuse";
      delay(1000);
      #ifdef DEBUGGING
      Serial.println("init in use");
      #endif
    }

    //Record workout
    recordWorkout();

    // Record movement (if movement is far away).
    if (!httpCall) {
      if (readNFC(serialString)) {
        // If workout has began, sync it with service
        if (repCount > 0 || setCount > 0) {
          if (syncWorkout()) {
            handleCheckOut();
          } else {
            lcdError("Something went wrong", "while syncing.", "Please try again.");
            delay(1000);
          }
        } else {
          handleCheckOut();
        }
      }
    }
  }
  // STATE SYNC, action: SEND WORKOUT DATA TO SERVER
  if (flowState == STATE_SYNC) {

  }
}
